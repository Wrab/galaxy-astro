// Daynums are used by the DayGameStats model and PlaySession analytics.
// They are an integer representing the days since the UNIX epoch, based on the UTC timezone.

export function getCurrentDaynum(): number {
	return Math.floor(Date.now() / 1000 / 60 / 60 / 24);
}

export function formatDaynum(daynum: number): string {
	const estDate = new Date(daynum * 1000 * 60 * 60 * 24);

	return estDate.toLocaleDateString("en-US", {
		day: "numeric",
		month: "short",
	});
}
