/**
 * This file is a modified version of
 * https://github.com/markdown-it/markdown-it/blob/master/lib/rules_inline/text.mjs .
 * Despite being supposed to treat pipe (|) as a special character, it does not.
 * This fucks with Discord-style spoilers.
 * 
 * This is so infuriating! But this works.
 * TODO move markdown stuff to its' own folder in `~/helper`
 */

function isTerminatorChar(ch) {
	switch (ch) {
		case 0x0a /* \n */:
		case 0x21 /* ! */:
		case 0x23 /* # */:
		case 0x24 /* $ */:
		case 0x25 /* % */:
		case 0x26 /* & */:
		case 0x2a /* * */:
		case 0x2b /* + */:
		case 0x2d /* - */:
		case 0x3a /* : */:
		case 0x3c /* < */:
		case 0x3d /* = */:
		case 0x3e /* > */:
		case 0x40 /* @ */:
		case 0x5b /* [ */:
		case 0x5c /* \ */:
		case 0x5d /* ] */:
		case 0x5e /* ^ */:
		case 0x5f /* _ */:
		case 0x60 /* ` */:
		case 0x7b /* { */:
		case 0x7c /* | -- this is my change. */:
		case 0x7d /* } */:
		case 0x7e /* ~ */:
			return true;
		default:
			return false;
	}
}

export default function text(state, silent) {
	let pos = state.pos;

	while (pos < state.posMax && !isTerminatorChar(state.src.charCodeAt(pos))) {
		pos++;
	}

	if (pos === state.pos) {
		return false;
	}

	if (!silent) {
		state.pending += state.src.slice(state.pos, pos);
	}

	state.pos = pos;

	return true;
}
