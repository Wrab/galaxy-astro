import { type ModLevel, hasStaffPerm } from "./adminHelper";
import type { ZodTypeAny, z } from "zod";
import { rdata, rfu, rmsg } from "./res";
import type { APIContext } from "astro";
import type { IUser } from "~/types";
import { keyv } from "~/models/keyv";

type MaybePromise<T> = T | Promise<T>;

/**
 * Route config.
 */
export type Config<Schema extends ZodTypeAny> = {
	/**
	 * Disable the route. You may provide a description for why the route is disabled.
	 */
	disabled?: boolean | string | (() => MaybePromise<boolean | string>);
	/**
	 * Require the user to be logged in and verified.
	 */
	auth?: boolean;
	/**
	 * If `true`, the user must not be muted.
	 *
	 * Only applied if `auth` is `true`.
	 */
	notMuted?: boolean;
	/**
	 * If `true`, the user must be over 13 years old.
	 * 
	 * Applied regardless of the value of `auth`.
	 */
	notUnderage?: boolean;
	/**
	 * The minimum moderator level that the user must have in order to use the route.
	 *
	 * Only applied if `auth` is `true`.
	 */
	reqLevel?: ModLevel;
	/**
	 * A Zod schema to use in parsing and validating the request body as JSON.
	 */
	schema?: Schema;
};

/**
 * Route context.
 *
 * This is just `APIContext` with some more specific types based on the config.
 */
export type Context<C extends Config<ZodTypeAny>> = APIContext &
	(C["auth"] extends true
		? {
				locals: {
					auth: {
						loggedIn: true;
						user: {
							muted: C["notMuted"] extends true ? false : boolean;
						};
					};
				};
			}
		: {});

export type Data<Schema extends ZodTypeAny> = z.output<Schema>;

export type RouteFunction<C> =
	C extends Config<infer Schema>
		? (context: Context<C>, data: Data<Schema>) => MaybePromise<Response>
		: never;

export function route<C extends Config<ZodTypeAny>>(
	config: C,
	fun: RouteFunction<C>
): (context: APIContext) => Promise<Response> {
	return async context => {
		const disabled =
			(typeof config.disabled === "function"
				? await config.disabled()
				: config.disabled) ?? false;
		if (typeof disabled === "string") {
			// Custom disabled message given
			return rmsg(disabled, 503);
		} else if (disabled) {
			// Automatic disabled message
			return rmsg("This route is disabled at this moment", 503);
		}

		const { user, loggedIn, isUnderage } = context.locals.auth;
		if (config.auth === true) {
			if (!loggedIn) {
				// Require the user to be logged in and verified
				return rfu();
			}

			if (config.notMuted === true && user.muted) {
				// Require the user to be unmuted
				return rfu("You are currently muted", 403);
			}

			if (config.reqLevel !== undefined && !hasStaffPerm(user.modLevel, config.reqLevel)) {
				// Require the user to be at least the given moderation level
				return rfu("Insufficient moderation permissions", 403);
			}
		}

		if (config.notUnderage === true && isUnderage) {
			return rfu("This API route is not available to underage users", 403);
		}

		if (config.schema) {
			// Schema specified; parse and validate
			const result = config.schema.safeParse(
				await context.request.json()
			);
			if (result.success === false)
				return rmsg(result.error.issues[0].message, 400);
			const data = result.data;

			return fun(context, data);
		} else {
			// Schema not specified; ignore
			return fun(context, undefined);
		}
	};
}

export function adminRoute<Schema extends ZodTypeAny>(
	{ reqLevel = "mod", schema }: { reqLevel?: ModLevel; schema?: Schema },
	fun: (
		request: Request,
		data: Data<Schema>,
		mod?: IUser
	) => MaybePromise<Response | void>
) {
	return route(
		{
			auth: true,
			reqLevel,
			schema,
		},
		async ({ request, locals }, data) => {
			const response = await fun(request, data, locals.auth.user);
			if (response) return response;
			return rmsg("Done");
		}
	);
}

export function adminDataRoute<T extends object, Schema extends ZodTypeAny>(
	{ reqLevel = "mod", schema }: { reqLevel?: ModLevel; schema?: Schema },
	fun: (request: Request, data: Data<Schema>) => MaybePromise<Response | T>
) {
	return route(
		{
			auth: true,
			reqLevel,
			schema,
		},
		async ({ request }, data) => {
			const response = await fun(request, data);
			if (response instanceof Response) return response;
			return rdata({ message: "Done", ...response });
		}
	);
}

/**
 * For use in the `disabled` config option.
 */
export function byLockdown(key: string, message?: string) {
	return async () => (await keyv.get(key, false)) && (message ?? true);
}
