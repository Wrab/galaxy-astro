import MarkdownIt from "markdown-it";
// import spoilerTag from "@traptitech/markdown-it-spoiler";
import spoilerTag from "./markdownSpoiler";

// add a render rule to set links to open in a new tab
const openLinksInNewTab = (md: MarkdownIt) => {
	const defaultRule =
		md.renderer.rules.link_open ??
		((tokens, idx, options, _env, self) => {
			return self.renderToken(tokens, idx, options);
		});

	md.renderer.rules.link_open = (tokens, idx, options, env, self) => {
		tokens[idx].attrSet("target", "_blank");
		return defaultRule(tokens, idx, options, env, self);
	};
};

// chat messages
export const chat = MarkdownIt({ linkify: true })
	.disable(["link", "image", "fence", "code", "hr", "list", "heading"])
	.use(openLinksInNewTab)
	.use(spoilerTag);

// comments
export const comment = MarkdownIt({ breaks: true })
	.disable(["link", "image", "fence", "code", "hr", "list", "heading"])
	.use(openLinksInNewTab)
	.use(spoilerTag);

// game descriptions
export const gameDescription = MarkdownIt({ linkify: true, breaks: true })
	.disable(["image"])
	.use(openLinksInNewTab)
	.use(spoilerTag);

// inbox messages (normal user)
export const message = MarkdownIt({ linkify: true, breaks: true })
	.disable(["link", "image"])
	.use(openLinksInNewTab)
	.use(spoilerTag);

// inbox messages (staff user)
export const staffMessage = MarkdownIt({ linkify: true, breaks: true })
	.disable(["image"])
	.use(openLinksInNewTab)
	.use(spoilerTag);

// update changelogs
export const updateChangelog = MarkdownIt({ breaks: true })
	.disable(["link", "image"])
	.use(openLinksInNewTab)
	.use(spoilerTag);

// update changelogs (syndication edition)
export const updateChangelogFeed = MarkdownIt({ breaks: true }).disable([
	"link",
	"image",
]);

// user bios
export const userBio = MarkdownIt({ linkify: true, breaks: true })
	.disable(["link", "image"])
	.use(openLinksInNewTab)
	.use(spoilerTag);

// forums posts
// TODO: links?
export const forumPost = MarkdownIt({ linkify: true, breaks: true })
	.disable(["link", "image"])
	.use(openLinksInNewTab)
	.use(spoilerTag);
