import type { IUser, StaffActivityType } from "~/types";
import { StaffActivity } from "~/models/staffactivity";

export type AdminOps = {
	counter: number;
	pushUsers(list: IUser[]): void;
	getUser(id: number): IUser | string | null;
	addModal(type: string, data: unknown): void;
	setTab(
		tab: string,
		data?: number | boolean,
		data2?: number | boolean
	): void;
	self: IUser;
};

export async function logAction(
	staff: number,
	type: StaffActivityType,
	summary: string,
	description: string,
	data: unknown
) {
	const activity = new StaffActivity({
		staff,
		type,
		summary,
		fullDescription: description,
		rawData: data,
	});

	await activity.save();
}

export type ModLevel = "any" | "verifier" | "mod" | "admin" | "owner";
const MOD_LEVELS: ModLevel[][] = [
	[],
	["verifier", "mod"],
	["verifier", "mod", "admin"],
	["verifier", "mod", "admin", "owner"],
	["verifier"],
];

export function hasStaffPerm(level: number, permission: ModLevel) {
	if (permission === "any") return level > 0;
	if (MOD_LEVELS[level] === undefined) return false;
	return MOD_LEVELS[level].includes(permission);
}

// For old systems that would be too hard to migrate to "hasStaffPerm"
// TODO: Ideally, this function should not be used.
export function effectiveModLevel(level: number) {
	if (level >= 0 && level <= 3) return level;
	return 0;
}
