import { createTRPCClient, createWSClient, wsLink } from "@trpc/client";
import type { SocketRouter } from "~/backend/router";
import SuperJSON from "superjson";

// // @ts-ignore
// export const client = generateRPCClient<SocketRouter, SocketRouter>({
// 	socketURL: "",
// 	transformer: superjson,
// });

export const client = createTRPCClient<SocketRouter>({
	links: [
		wsLink({
			transformer: SuperJSON,
			client: createWSClient({
				url: `${
					window.location.protocol === "https:" ? "wss" : "ws"
				}://${location.hostname}:8443/api/trpc`,
				lazy: {
					// Only open the WebSocket when it is needed, close it when it is not.
					enabled: true,
					closeMs: 30000,
				},
			}),
		}),
	],
});

// export const useQuery = client.useQuery;
// export const useMutation = client.useMutation;
// export const runQuery = client.runQueryAndThrow;
// export const runMutation = client.runMutationAndThrow;
// export const subscribe = client.subscribe;
