import type { Plugin, ViteDevServer } from "vite";
import type { AnyTRPCRouter } from "@trpc/server";
import type { AstroIntegration } from "astro";
import { WebSocketServer } from "ws";
import { applyWSSHandler } from "@trpc/server/adapters/ws";

type Options = {
	port: number;
	routerFilePath: string;
	routerName: string;
	createContextName: string;
};

export default function createIntegration(options: Options): AstroIntegration {
	let wss: WebSocketServer;
	let handler: ReturnType<typeof applyWSSHandler>;

	return {
		name: "trpc-wss",
		hooks: {
			"astro:config:setup": async ({ command, logger, updateConfig }) => {
				if (command === "dev") {
					const reload = async (server: ViteDevServer) => {
						const router = await server.ssrLoadModule(
							options.routerFilePath,
							{
								fixStacktrace: true,
							}
						);

						// Remove any listeners previously added by TRPC while applying the handler.
						wss.removeAllListeners();

						// Re-apply the handler.
						handler = applyWSSHandler({
							wss,
							router: router[options.routerName] as AnyTRPCRouter,
							createContext: router[options.createContextName],
							onError: err => {
								logger.error("Router error.");
								console.error(err);
							},
						});

						// Re-add *our* listener that was just removed.
						wss.on("connection", ws => {
							logger.info(
								`Client connected [total: ${wss.clients.size}]`
							);
							ws.once("close", () => {
								logger.info(
									`Client disconnected [total: ${wss.clients.size}]`
								);
							});
						});
					};

					// Add a Vite plugin to manage the dev server and handle HMR.
					const plugin: Plugin = {
						name: "trpc-wss",
						apply: "serve",
						configureServer: server => {
							// We run everything in the post hook here because `ssrLoadModule` does not resolve modules correctly in the pre hook.
							return async () => {
								wss = new WebSocketServer({
									host: "0.0.0.0",
									port: options.port,
								});

								wss.once("listening", async () => {
									logger.info("Loading router...");
									await reload(server);
									logger.info(
										`WebSocket server ready on http://localhost:${options.port}/`
									);
								});
							};
						},
						handleHotUpdate: async ({ server }) => {
							logger.info("Reloading router...");
							await reload(server);
						},
					};
					updateConfig({
						vite: {
							plugins: [plugin],
						},
					});
				}
			},
			"astro:server:done": ({ logger }) => {
				logger.info("Closing WebSocket server...");

				handler.broadcastReconnectNotification();
				wss.close();

				logger.info("WebSocket server closed.");
			},
		},
	};
}
