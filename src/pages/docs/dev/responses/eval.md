---
layout: ~/layouts/Doc.astro
title: galaxy docs - eval response
---

# `eval`

The `eval` response is sent when galaxy would like to run code in your game's scope.

Although it is not required to implement this functionality in your game, it is highly recommended you do. At the time of writing this, galaxy does not use this response, but in the future it will allow for features such as auto-clickers and userscripts.

## properties

- **`code`** *(string)*  
  The code to be ran through `eval` or a similar function.

## example

```js
{
	type: "eval",
	code: "console.log('hello world');"
}
```
