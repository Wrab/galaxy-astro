import { getTagData } from "~/helper/tags";
import { rdata } from "~/helper/res";

export async function GET() {
	return rdata(getTagData(), 200);
}
