import { hasStaffPerm, logAction } from "~/helper/adminHelper";
import { User } from "~/models/user";
import { adminRoute } from "~/helper/route";
import { rmsg } from "~/helper/res";
import { z } from "zod";

const schema = z.object({
	id: z.number().int(),
	name: z.string().optional(),
	bio: z.string().optional(),
	flairs: z.array(z.string()).optional(),
	equippedFlair: z.string().optional(),
	modLevel: z.number().optional(),
	banned: z.boolean().optional(),
	muted: z.boolean().optional(),
});

export const POST = adminRoute({ schema }, async (_request, editing, mod) => {
	if (editing.id === mod.id && !hasStaffPerm(mod.modLevel, "admin"))
		return rmsg("You can't edit yourself, silly!", 403);
	const u = await User.findOne({ id: editing.id });
	if (!u) return rmsg("User not found", 404);

	let editedSummary = "";

	for (const prop in editing) {
		// Surely it's a good idea to silently fail here
		if (prop === "modLevel" && !hasStaffPerm(mod.modLevel, "owner")) continue;

		// Limit name changes to admins, and reject changes that would cause a duplicate name
		if (
			prop === "name" &&
			(!hasStaffPerm(mod.modLevel, "admin") ||
				(await User.findOne({ name: editing.name })) !== null)
		)
			continue;

		// I can't just do a raw !== here because flairs are an array :pensive:
		if (JSON.stringify(u[prop]) !== JSON.stringify(editing[prop])) {
			editedSummary += `${prop}: ${u[prop]} -> ${editing[prop]}\n`;
			u[prop] = editing[prop];
		}
	}

	await u.save();

	console.log(editedSummary);

	if (editedSummary !== "")
		await logAction(
			mod.id,
			"user",
			`updated info for user ${u.name} (${u.id})`,
			editedSummary.trim(),
			{ userId: u.id, editing }
		);
});
