import { Comment } from "~/models/comment";
import { Game } from "~/models/game";
import { User } from "~/models/user";
import { adminRoute } from "~/helper/route";
import { logAction } from "~/helper/adminHelper";
import { rmsg } from "~/helper/res";
import { z } from "zod";

const schema = z.object({
	id: z.number().int(),
	content: z.string().max(4096),
	devResponse: z.string().max(8096).optional(),
});

export const POST = adminRoute({ schema }, async (_request, editing, mod) => {
	const c = await Comment.findOne({ id: editing.id });
	if (!c) return rmsg("Comment not found", 404);

	// Nothing needs to be done here :>
	if (editing.content === c.content && editing.devResponse === c.devResponse)
		return;

	let editedSummary = "";

	// I'll do this manually here rather than a loop like the other bulk edit endpoints
	// because I'm not sure how nice everything will play with the intentional desire
	// to set `devResponse` to `undefined`
	// TODO: Would it be better to pass `false` through the API instead?
	if (editing.content !== c.content) {
		editedSummary += `content: ${c.content} -> ${editing.content}`;
		c.content = editing.content;
	}

	if (editing.devResponse !== c.devResponse) {
		editedSummary += `dev response: ${c.devResponse} -> ${editing.devResponse}`;
		c.content = editing.content;
	}

	await c.save();

	const user = await User.findOne({ id: c.author });
	const game = await Game.findOne({ id: c.game });
	editedSummary = `on game ${game.name} (${game.id})\n` + editedSummary;
	await logAction(
		mod.id,
		"comment",
		`edited comment by ${user.name} (${user.id})`,
		editedSummary.trim(),
		{ userId: user.id, editing, comment: c }
	);
});
