import { StaffNote } from "~/models/staffnote";
import { adminRoute } from "~/helper/route";
import { logAction } from "~/helper/adminHelper";
import { z } from "zod";
import { UserAuth } from "~/models/userauth";

const schema = z.object({
	content: z.string(),
	id: z.string(),
});

export const POST = adminRoute(
	{ reqLevel: "verifier", schema },
	async (_request, editing, mod) => {
		let note = await StaffNote.findOne({ id: editing.id });
		if (!note)
			note = new StaffNote({
				content: editing.content,
				id: editing.id,
				authors: [mod.id],
			});
		else {
			note.content = editing.content;
			if (!note.authors.includes(mod.id)) note.authors.push(mod.id);
		}

		await note.save();

		await logAction(
			mod.id,
			"note",
			`edited note ${editing.id}`,
			`new content:
${editing.content}`,
			{ note, editing }
		);
	}
);
