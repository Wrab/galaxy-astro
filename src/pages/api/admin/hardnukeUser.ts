import { ChatMessage } from "~/models/chatmessage";
import { Comment } from "~/models/comment";
import { Favorite } from "~/models/favorite";
import { Game } from "~/models/game";
import { Message } from "~/models/message";
import { PlaySession } from "~/models/playsession";
import { Playtime } from "~/models/playtime";
import { Rating } from "~/models/rating";
import { StaffNote } from "~/models/staffnote";
import { User } from "~/models/user";
import { adminRoute } from "~/helper/route";
import { rmsg } from "~/helper/res";
import sharp from "sharp";
import { updatePfps } from "~/helper/img";
import { z } from "zod";

const schema = z.object({
	user: z.number().int(),
});

export const POST = adminRoute({ schema }, async (_request, { user }) => {
	const u = await User.findOne({ id: user });
	if (!u) return rmsg("User not found", 404);

	const originalName = u.name;

	u.banned = true;
	u.name = "[removed]";
	u.bio = "[removed]";

	const original = u.pfpTimestamp;
	u.pfpTimestamp = Date.now();
	u.playMinutes = 0;
	await u.save();

	await Game.deleteMany({ author: user });
	await Comment.updateMany({ author: user }, { deleted: true });
	await ChatMessage.updateMany({ author: user }, { deleted: true });
	await Message.updateMany({ from: user }, { deleted: true });
	await Rating.deleteMany({ author: user });
	await Favorite.deleteMany({ user });
	await updatePfps(
		sharp("public/unknown-user.webp"),
		user,
		u.pfpTimestamp,
		original
	);

	const playtimes = await Playtime.find({ user });
	const plays = await PlaySession.aggregate([
		{ $match: { user, minutes: { $gte: 3 } } },
		{ $group: { _id: "$game", plays: { $count: {} } } },
	]);

	const awaits = [
		...playtimes.map(p =>
			Game.updateOne(
				{ id: p.game },
				{ $inc: { playMinutes: -p.minutes } }
			)
		),
		...plays.map(p =>
			Game.updateOne({ id: p.game }, { $inc: { plays: -p.count } })
		),
	];

	await Promise.all(awaits);
	await Playtime.deleteMany({ user });

	let staffNote = await StaffNote.findOne({ id: `user-${u.id}` });
	if (staffNote === null) {
		staffNote = new StaffNote({
			id: `user-${u.id}`,
			content: `formerly known as ${originalName}`,
			authors: [-1],
		});
	} else {
		staffNote.authors.push(-1);
		staffNote.content += `\n\nformerly known as ${originalName}`
	}

	await staffNote.save();
});
