import { CategoryRequest } from "~/models/categoryrequest";
import { Game } from "~/models/game";
import { GameRequest } from "~/models/gamerequest";
import { Report } from "~/models/report";
import { adminDataRoute } from "~/helper/route";

export const GET = adminDataRoute({ reqLevel: "any" }, async () => {
	const unverified = await Game.find({ verified: { $ne: true } });
	const unresolved = await Report.find({ resolved: { $ne: true } });
	const requests = await GameRequest.find({ resolved: false });
	const crequests = await CategoryRequest.find({ resolved: false });

	return { unverified, unresolved, requests, crequests };
});
