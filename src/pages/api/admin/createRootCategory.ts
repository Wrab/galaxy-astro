// I don't think this endpoint is needed for the time being, but I'll leave it in the codebase.
// TODO: Does this need to be deleted?

import { CategoryModerator } from "~/models/categorymod";
import { ForumCategory } from "~/models/forumcategory";
import { adminRoute } from "~/helper/route";
import { keyv } from "~/models/keyv";
import { z } from "zod";

const schema = z.object({
	name: z.string(),
	slug: z.string(),
	owner: z.number().int(),
});

export const POST = adminRoute(
	{
		schema,
	},
	async (_request, { name, slug, owner }) => {
		const id = await keyv.id("lastCategoryId");
		const cat = new ForumCategory({
			id,
			slug,
			name,
			parent: false,
			locked: true,
		});
		await cat.save();

		const permissionNode = new CategoryModerator({
			perms: 3,
			user: owner,
			category: id,
		});
		await permissionNode.save();
	}
);
