import sendWebhook, { sendFancyWebhook } from "~/helper/sendWebhook";
import { Game } from "~/models/game";
import { Message } from "~/models/message";
import { User } from "~/models/user";
import { adminRoute } from "~/helper/route";
import { changeThumbIds } from "~/helper/img";
import { convertFormat } from "~/libgalaxy";
import { env } from "~/helper/env";
import { keyv } from "~/models/keyv";
import { logAction } from "~/helper/adminHelper";
import { rmsg } from "~/helper/res";
import { z } from "zod";

const schema = z.object({
	game: z.number().int(),
	status: z.boolean().or(z.string()),
});

export const POST = adminRoute(
	{ reqLevel: "verifier", schema },
	async (_request, { game, status }, mod) => {
		// Update verification status
		await Game.updateOne({ id: game }, { verified: status });

		const gameDoc = await Game.findOne({ id: game });
		if (!gameDoc) return rmsg("Game not found", 404);

		const user = await User.findOne({ id: gameDoc.author });

		await logAction(
			mod.id,
			"game",
			`set verification status for ${gameDoc.name} (${game}) to ${status}`,
			`game by ${user.name} (${user.id})`,
			{ game: gameDoc, userId: user.id }
		);

		// 0 play-minutes as a heuristic to see if the game is a re-verification or not.
		// No point in congratulating the author and bumping game if the link has been changed,
		// rather than a full update
		if (gameDoc.playMinutes > 0) {
			const msg = new Message({
				from: -1,
				to: gameDoc.author,
				title: `${gameDoc.name} was re-verified`,
				content: `
Your game ${gameDoc.name} has been re-verified after changing the link. It is now once again accessible to everyone else on galaxy.
			
You can see it for yourself [here](/play/${gameDoc.id}).`.trim(),
			});
			await msg.save();

			const unv = await Game.countDocuments({ verified: { $ne: true } });
			if (env.STAFF_GAME_WEBHOOK !== undefined)
				await sendWebhook(env.STAFF_GAME_WEBHOOK, {
					content: `${gameDoc.name} has been re-verified. ${unv} left.`,
				});
		} else if (status === true) {
			// New unverified games start with a negative ID, so they don't
			// take up regular IDs and leave gaps when rejected. This migrates the
			// game to a positive ID, by giving it the id and moving the thumbnail.
			if (gameDoc.id < 0) {
				const prevId = gameDoc.id;
				gameDoc.id = await keyv.id("lastGameId");
				await gameDoc.save();
				await changeThumbIds(
					prevId,
					gameDoc.id,
					gameDoc.thumbTimestamp
				);
			}

			gameDoc.createdAt = new Date();
			gameDoc.lastUpdate = Date.now();
			await gameDoc.save();

			// Add "gamedev" flair if user does not already have it
			let isFlairAwarded = false;
			if (!user.flairs.includes("gamedev")) {
				user.flairs.push("gamedev");
				await user.save();
				isFlairAwarded = true;
			}

			// Notify the user that their game has been verified
			const msg = new Message({
				from: -1,
				to: gameDoc.author,
				title: `${gameDoc.name} was verified`,
				content: `
Your game ${gameDoc.name} has been verified. Now anyone on galaxy can play it!
			
You can see it for yourself [here](/play/${gameDoc.id}).

${isFlairAwarded ? "You have also received the gamedev flair!" : ""}`.trim(),
			});
			await msg.save();

			const unv = await Game.countDocuments({ verified: { $ne: true } });
			if (env.STAFF_GAME_WEBHOOK !== undefined)
				await sendWebhook(env.STAFF_GAME_WEBHOOK, {
					content: `${gameDoc.name} has been verified. ${unv} left.`,
				});

			if (env.GAME_WEBHOOK !== undefined)
				await sendFancyWebhook(env.GAME_WEBHOOK, {
					username: user.name,
					avatar_url: convertFormat(
						"https://galaxy.click/pfp/large/",
						user.id,
						user.pfpTimestamp
					),
					embeds: [
						{
							title: gameDoc.name,
							description: gameDoc.description,
							url: `https://galaxy.click/play/${gameDoc.id}`,
							color: 4723016,
							footer: {
								text: "new game verified",
							},
							timestamp: new Date().toISOString(),
							image: {
								url: convertFormat(
									"https://galaxy.click/thumb/large/",
									gameDoc.id,
									gameDoc.thumbTimestamp
								),
							},
							fields: [
								{
									name: "tags",
									value:
										gameDoc.tags.join(", ") ?? "[no tags]",
									inline: true,
								},
								{
									name: "direct link",
									value: gameDoc.link,
									inline: true,
								},
							],
						},
					],
				});
		} else {
			// Remove "gamedev" flair if user has no verified games
			const gamesVerified = await Game.countDocuments({
				author: gameDoc.author,
				verified: true,
			});
			if (gamesVerified === 0 && user.flairs.includes("gamedev")) {
				user.flairs.splice(user.flairs.indexOf("gamedev"), 1);
				await user.save();
			}

			const unv = await Game.countDocuments({ verified: { $ne: true } });
			if (env.STAFF_GAME_WEBHOOK !== undefined)
				await sendWebhook(env.STAFF_GAME_WEBHOOK, {
					content: `${gameDoc.name} verification note changed to \`${status}\`. ${unv} left.`,
				});
		}
	}
);
