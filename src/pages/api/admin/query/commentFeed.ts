import { Comment } from "~/models/comment";
import { adminDataRoute } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	page: z.number().int().min(1).default(1),
	user: z.number().int().optional(),
	sort: z.enum(["-id", "score"]),
});

export const POST = adminDataRoute(
	{
		schema,
	},
	async (_request, data) => {
		const query = data.user !== undefined ? { author: data.user } : {};
		const pageCount = Math.ceil(
			(await Comment.countDocuments(query)) / 100
		);
		const result = (
			await Comment.find(query)
				.sort(data.sort)
				.limit(100)
				.skip(100 * (data.page - 1))
		).map(x => x.toObject());

		return { result, pageCount };
	}
);
