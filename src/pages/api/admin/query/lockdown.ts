import { adminDataRoute } from "~/helper/route";
import { keyv } from "~/models/keyv";

export const GET = adminDataRoute({}, async () => {
	const [
		comments,
		games,
		updates,
		users,
		feedback,
		messages,
		chat,
		forum,
	] = await Promise.all([
		keyv.get("commentsLocked", false),
		keyv.get("gamesLocked", false),
		keyv.get("updatesLocked", false),
		keyv.get("usersLocked", false),
		keyv.get("feedbackLocked", false),
		keyv.get("messagesLocked", false),
		keyv.get("chatLocked", false),
		keyv.get("forumLocked", false),
	]);

	return {
		data: {
			comments,
			games,
			updates,
			users,
			feedback,
			messages,
			chat,
			forum,
		},
	};
});
