import { User } from "~/models/user";
import { UserAuth } from "~/models/userauth";
import { adminDataRoute } from "~/helper/route";
import { z } from "zod";

export const POST = adminDataRoute(
	{
		reqLevel: "verifier",
		schema: z.number().int().or(z.string()),
	},
	async (_request, idOrName) => {
		const user = await User.findOne(
			typeof idOrName === "string" ? { name: idOrName } : { id: idOrName }
		);
		const userAuth = await UserAuth.findOne({ id: user.id });

		// .toObject() bypasses the toJSON sanitization. Scary! I remember when this ruined a month of my life.
		return {
			user: { ...user.toObject(), parentState: userAuth.parentState },
		};
	}
);
