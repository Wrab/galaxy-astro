import { ChatMessage } from "~/models/chatmessage";
import { adminDataRoute } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	page: z.number().int().min(1).default(1)
});

export const POST = adminDataRoute(
	{
		schema,
	},
	async (_request, data) => {
		const pageCount = Math.ceil((await ChatMessage.estimatedDocumentCount()) / 100);
		const result = (await ChatMessage.find({}).sort("-createdAt").limit(100).skip(100 * (data.page - 1))).map(
			x => x.toObject()
		);

		return { result, pageCount };
	}
);
