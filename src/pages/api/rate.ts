import { byLockdown, route } from "~/helper/route";
import { rdata, rmsg } from "~/helper/res";
import { Game } from "~/models/game";
import { Rating } from "~/models/rating";
import { z } from "zod";

const schema = z.object({
	id: z.number().int(),
	rating: z.number().int().min(1, "Invalid rating").max(5, "Invalid rating"),
});

export const POST = route(
	{
		disabled: byLockdown(
			"feedbackLocked",
			"Game feedback is disabled at this moment."
		),
		auth: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		// Find the game
		const game = await Game.findOne({ id: req.id });
		if (!game || game.verified !== true)
			return rmsg("Attempted to rate unknown game", 400);

		if (game.author === user.id)
			return rmsg("You can not rate your own game", 400);

		const author = user.id;

		// Check if rating already exists
		let rating = await Rating.findOne({
			game: game.id,
			author,
		});

		const stars = req.rating;

		let ratingDeleted = false;
		if (rating) {
			if (rating.rating === stars) {
				// Remove rating
				// game.ratingValue -= rating.rating;
				// game.ratingCount--;
				await rating.deleteOne();
				ratingDeleted = true;
			} else {
				// Update rating
				// game.ratingValue -= rating.rating;
				rating.rating = stars;
			}
		} else {
			// Create rating
			rating = new Rating({
				game: game.id,
				author,
				rating: stars,
			});

			// game.ratingCount++;
		}

		if (!ratingDeleted) game.ratingValue += stars;

		if (!ratingDeleted) await rating.save();
		const query = await Rating.aggregate([
			{ $match: { game: game.id } },
			{
				$group: {
					_id: game.id,
					sum: { $sum: "$rating" },
					count: { $count: {} },
				},
			},
		]);

		if (query.length) {
			game.ratingCount = query[0].count;
			game.ratingValue = query[0].sum;
		} else {
			game.ratingCount = 0;
			game.ratingValue = 0;
		}

		if (game.ratingCount === 0) game.ratingAvg = 0;
		else game.ratingAvg = game.ratingValue / game.ratingCount;

		// "The usual trick for this is to add a 1 star and a 5 star review.
		// It'll have more impact on scores with fewer "real" votes, bringing
		// them closer to 3.0, but will basically not touch sufficiently rated
		// scores at all" -ThePaperPilot
		game.sortRating = (game.ratingValue + 6) / (game.ratingCount + 2);

		// game.sortRating = game.ratingAvg + Math.log10(Math.min(game.ratingCount, 100))

		await game.save();

		return rdata(
			{
				message: "success",
				rating: game.ratingAvg,
				count: game.ratingCount,
			},
			200
		);
	}
);
