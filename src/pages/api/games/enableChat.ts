import { rfu, rmsg } from "~/helper/res";
import { Channel } from "~/models/channel";
import { Game } from "~/models/game";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	id: z.number().int(),
});

export const POST = route(
	{
		auth: true,
		notMuted: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const game = await Game.findOne({ id: req.id });
		if (!game) return rmsg("Game not found", 404);

		if (game.author !== user.id)
			return rfu("You are not the author of this game", 403);
		if (game.verified !== true)
			return rmsg(
				"You can not enable chat before a game is verified",
				400
			);
		if (game.chatEnabled === true)
			return rmsg("Chat is already enabled", 405);

		game.chatEnabled = true;
		const channel = new Channel({
			id: `game-${game.id}`,
			name: game.name,
			owner: game.author,
		});
		await game.save();
		await channel.save();

		return rmsg("Chat enabled!");
	}
);
