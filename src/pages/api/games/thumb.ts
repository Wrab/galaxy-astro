import { rdata, rfu, rmsg } from "~/helper/res";
import { Game } from "~/models/game";
import { route } from "~/helper/route";
import sharp from "sharp";
import { updateThumbs } from "~/helper/img";

export const POST = route(
	{
		auth: true,
		notMuted: true,
	},
	async ({ locals, request }) => {
		const { user } = locals.auth;

		const req = await request.formData();

		const gameId = parseInt(req.get("id"));
		if (isNaN(gameId)) return rmsg("Invalid game ID", 400);

		const file = req.get("image");
		if (file === null) return rmsg("No image provided", 400);
		if (typeof file === "string") return rmsg("Invalid image", 400);
		if (file.size > 1024 * 1024 * 8)
			return rmsg("Please submit an image under 8 MB", 400);

		const game = await Game.findOne({ id: gameId });
		if (!game) return rmsg("Game not found", 404);

		if (game.author !== user.id)
			return rfu("You are not the author of this game", 403);

		// This is super stupid.
		// To put it simply, I hate this code.
		// But, alas, it works.
		//   File -> ArrayBuffer -> Buffer -> Sharp
		const abuf = await file.arrayBuffer();
		const buf = Buffer.from(abuf);

		let oops = false;

		const original = game.thumbTimestamp;
		game.thumbTimestamp = Date.now();
		await game.save();

		await updateThumbs(
			sharp(buf),
			game.id,
			game.thumbTimestamp,
			original
		).catch(e => {
			oops = e;
		});
		if (oops)
			return rmsg(
				`Something went wrong with image processing. ${oops}`,
				500
			);

		return rdata({ message: "success", time: game.thumbTimestamp });
	}
);
