import { getLevel, getXP, getXPToNextLevel } from "~/helper/leveling";
import { rdata, rmsg } from "~/helper/res";
import { DayGameStats } from "~/models/daygamestats";
import { Game } from "~/models/game";
import { Message } from "~/models/message";
import { PlaySession } from "~/models/playsession";
import { Playtime } from "~/models/playtime";
import { User } from "~/models/user";
import { getCurrentDaynum } from "~/helper/daynums";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	game: z.number().int(),
});

export const POST = route(
	{
		auth: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const game = await Game.findOne({ id: req.game });
		if (!game || game.verified !== true) return rmsg("Game not found", 404);

		// Play session system

		let session = await PlaySession.findOne({
			game: game.id,
			user: user.id,
			endTime: { $gt: Date.now() - 1000 * 60 * 30 },
		});
		let newSession = false;
		if (!session) {
			// Start new play session
			session = new PlaySession({
				game: game.id,
				user: user.id,
				minutes: 1,
			});
			newSession = true;

			await session.save();
		}

		if (Date.now() - session.endTime > 58 * 1000 || newSession) {
			if (!newSession)
				await PlaySession.updateOne(
					{
						game: game.id,
						user: user.id,
						startTime: session.startTime,
					},
					{ $inc: { minutes: 1 }, $set: { endTime: Date.now() } }
				);

			// === 2 because it's just been incremented to 3
			if (session.minutes === 2) {
				await Game.updateOne({ id: game.id }, { $inc: { plays: 1 } });
				await DayGameStats.updateOne(
					{ game: game.id, day: getCurrentDaynum() },
					{ $inc: { minutes: 1, plays: 1 } },
					{ upsert: true }
				);
			} else {
				await DayGameStats.updateOne(
					{ game: game.id, day: getCurrentDaynum() },
					{ $inc: { minutes: 1 } },
					{ upsert: true }
				);
			}
		}
		// Update notifications count
		let mcount: string | number = "0";
			mcount = await Message.countDocuments({
				to: user.id,
				read: false,
				deleted: false,
			});
			mcount = mcount.toLocaleString();
		// Original playtime system

		let playtime = await Playtime.findOne({ game: game.id, user: user.id });

		if (!playtime) {
			// First heartbeat for game
			playtime = new Playtime({
				game: game.id,
				user: user.id,
				minutes: 1,
			});
		} else {
			if (Date.now() - user.lastHeartbeat < 59 * 1000) {
				return rdata({ message: "Invalid heartbeat", notification: mcount }, 400);
			}
			// if (playtime.updatedAt)

			// Playtime exists
			playtime.minutes++;
		}

		await User.updateOne(
			{ id: user.id },
			{
				$inc: { playMinutes: 1 },
				lastHeartbeat: Date.now(),
			}
		);
		await Game.findOneAndUpdate(
			{ id: game.id },
			{ $inc: { playMinutes: 1 } }
		);
		await playtime.save();

		user.playMinutes++;

		const xp = getXP(user);
		const level = getLevel(xp);
		const xpToCurrent = getXPToNextLevel(level - 1);
		const xpToNext = getXPToNextLevel(level);

		return rdata(
			{ level, xp: xp - xpToCurrent, next: xpToNext - xpToCurrent, notification: mcount },
			201
		);
	}
);
