import { byLockdown, route } from "~/helper/route";
import { rdata, rmsg } from "~/helper/res";
import { Favorite } from "~/models/favorite";
import { Game } from "~/models/game";
import { z } from "zod";

const schema = z.object({
	game: z.number().int(),
});

export const POST = route(
	{
		disabled: byLockdown(
			"feedbackLocked",
			"Game feedback is disabled at this moment."
		),
		auth: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const game = await Game.findOne({ id: req.game });
		if (game === null || game.verified !== true)
			return rmsg("Game not found", 404);

		let fave = await Favorite.findOne({ game: game.id, user: user.id });

		if (fave) {
			// Already favorited, delete it
			await fave.deleteOne();
			game.favorites = await Favorite.countDocuments({ game: game.id });
			await game.save();
			return rdata({
				message: "Favorite removed",
				faved: false,
				faves: game.favorites,
			});
		}

		fave = new Favorite({
			game: game.id,
			user: user.id,
		});
		await fave.save();
		game.favorites = await Favorite.countDocuments({ game: game.id });
		await game.save();

		return rdata({
			message: "Favorite added",
			faved: true,
			faves: game.favorites,
		});
	}
);
