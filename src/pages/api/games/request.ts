import { GameRequest } from "~/models/gamerequest";
// import { env } from "~/helper/env";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
// import sendWebhook from "~/helper/sendWebhook";
import { z } from "zod";

const schema = z.object({
	name: z.string().trim().max(256),
	link: z.string().trim().url().max(512),
	contact: z.string().max(512),
	also: z.string().trim().max(2048),
});

export const POST = route(
	{ disabled: ":(", auth: true, notMuted: true, schema },
	async ({ locals }, { name, link, contact, also }) => {
		const { user } = locals.auth;

		const doc = new GameRequest({
			name,
			link,
			contact,
			also,
			requestAuthor: user.id,
		});
		await doc.save();

		// 		if (env.STAFF_WEBHOOK)
		// 			await sendWebhook(env.STAFF_WEBHOOK, {
		// 				content: `new game ${name} (${link}) has been requested
		// <https://galaxy.click/admin/housekeeping>`,
		// 				name: "game request",
		// 			});

		return rmsg("success");
	}
);
