import { CategoryRequest } from "~/models/categoryrequest";
import { ForumCategory } from "~/models/forumcategory";
import { env } from "~/helper/env";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import sendWebhook from "~/helper/sendWebhook";
import { z } from "zod";

const schema = z.object({
	name: z
		.string()
		.trim()
		.nonempty("Please specify a name")
		.max(128, "Category name too long"),
	slug: z
		.string()
		.trim()
		.nonempty("Please specify a slug")
		.max(32, "Category slug too long"),
	description: z
		.string()
		.nonempty("Please specify a description")
		.max(512, "Description too long"),
	reason: z.string().trim().nonempty("Please specify a reason").max(2048),
});

export const POST = route(
	{ auth: true, notMuted: true, schema },
	async ({ locals }, req) => {
		const { user } = locals.auth;
		const { name, slug, description, reason } = req;

		const alreadyRequested = await CategoryRequest.findOne({
			slug,
			resolved: false,
		});
		if (alreadyRequested)
			return rmsg(
				"A category with this slug has already been requested.",
				400
			);

		const alreadyExists = await ForumCategory.findOne({ slug });
		if (alreadyExists)
			return rmsg("A category with this slug already exists.", 400);

		const doc = new CategoryRequest({
			name,
			slug,
			description,
			reason,
			requestAuthor: user.id,
		});

		await doc.save();

		if (env.STAFF_REPORT_WEBHOOK !== undefined)
			await sendWebhook(env.STAFF_REPORT_WEBHOOK, {
				content: `new category ${name} (${slug}) has been requested
<https://galaxy.click/admin>`,
				name: "category request",
			});

		return rmsg("success");
	}
);
