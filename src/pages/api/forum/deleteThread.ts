import { rdata, rmsg } from "~/helper/res";
import { ForumCategory } from "~/models/forumcategory";
import { ForumThread } from "~/models/forumthread";
import { forumModeratorLevel } from "~/helper/forum";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.number().int();

export const POST = route(
	{ auth: true, notMuted: true, schema },
	async ({ locals }, threadId) => {
		const { user } = locals.auth;

		const t = await ForumThread.findOne({
			id: threadId,
		});
		if (!t) return rmsg("Thread not found", 400);

		const category = await ForumCategory.findOne({ id: t.category });
		const modLevel = await forumModeratorLevel(user, category);

		if (modLevel < 1) {
			return rmsg(
				"sometimes i write these error messages wondering if anyone will ever read them. is there a purpose to this madness, or do i pointlessly sit here banging away code for features that nobody will ever use for some niche site?",
				400
			);
		}

		t.deleted = true;
		t.save();

		return rdata({ message: "ok" });
	}
);
