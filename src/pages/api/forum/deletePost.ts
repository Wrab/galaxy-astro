import { rdata, rmsg } from "~/helper/res";
import { ForumPost } from "~/models/forumpost";
import { ForumThread } from "~/models/forumthread";
import { hasStaffPerm } from "~/helper/adminHelper";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	post: z.number().int(),
});

export const POST = route(
	{ auth: true, notMuted: true, schema },
	async ({ locals }, { post: postId }) => {
		const { user } = locals.auth;

		const p = await ForumPost.findOne({
			id: postId,
		});
		if (!p) return rmsg("Post not found", 400);

		if (p.author != user.id && !hasStaffPerm(user.modLevel, "mod")) {
			return rmsg(
				"You can't just delete someone else's post like that!!!! :<",
				400
			);
		}

		p.deleted = true;
		p.save();

		// Remove post solution if it was the solution
		await ForumThread.updateOne(
			{ solution: postId },
			{ $set: { solution: false } }
		);

		return rdata({ message: "ok" });
	}
);
