import { byLockdown, route } from "~/helper/route";
import { rdata, rmsg } from "~/helper/res";
import { ForumCategory } from "~/models/forumcategory";
import { ForumPost } from "~/models/forumpost";
import { ForumThread } from "~/models/forumthread";
import { forumModeratorLevel } from "~/helper/forum";
import { keyv } from "~/models/keyv";
import { z } from "zod";

const schema = z.object({
	title: z
		.string()
		.trim()
		.nonempty("Title is required")
		.max(128, "Thread title can only be up to 128 characters"),
	body: z
		.string()
		.trim()
		.max(16384, "Post body can not exceed 16,384 characters"),
	category: z.number().int(),
});

export const POST = route(
	{
		auth: true,
		notMuted: true,
		notUnderage: true,
		schema,
		disabled: byLockdown(
			"forumLocked",
			"The forums are disabled at this moment."
		),
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;
		const { title, body, category } = req;

		if (user.playMinutes < 5)
			return rmsg(
				"Your account is too new to make a forum thread! Try playing some games first.",
				400
			);

		const cat = await ForumCategory.findOne({ id: category });
		if (!cat) return rmsg("Category not found", 400);

		// TODO allow posting in locked threads if moderator
		if (
			cat.locked !== false &&
			(await forumModeratorLevel(user, cat)) === 0
		)
			return rmsg(
				"This category is locked at the moment, try again later.",
				400
			);

		const timestamp = new Date();
		const threadId = await keyv.id("lastThreadId");
		const postId = await keyv.id("lastPostId");

		const thread = new ForumThread({
			author: user.id,
			category: cat.id,
			id: threadId,
			when: timestamp,
			lastPost: timestamp,
			preview: body.slice(0, 200),
			name: title,
			postCount: 1,
		});
		await thread.save();

		const post = new ForumPost({
			author: user.id,
			body,
			id: postId,
			when: timestamp,
			thread: threadId,
		});

		await Promise.all([
			post.save(),
			ForumCategory.updateOne(
				{ id: cat.id },
				{
					$set: { lastPost: timestamp, lastThread: timestamp },
					$inc: { postCount: 1, threadCount: 1 },
				}
			),
		]);

		return rdata({ message: ":+1:", thread: threadId });
	}
);
