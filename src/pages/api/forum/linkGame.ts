import { ForumCategory } from "~/models/forumcategory";
import { Game } from "~/models/game";
import { forumModeratorLevel } from "~/helper/forum";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	category: z.number().int().min(1),
	game: z.number().int().min(1),
});

export const POST = route(
	{ auth: true, notMuted: true, schema },
	async ({ locals }, req) => {
		const { user } = locals.auth;
		const { category: categoryId, game: gameId } = req;

		const category = await ForumCategory.findOne({ id: categoryId });
		if (!category) return rmsg("Category not found", 400);

		const modLevel = await forumModeratorLevel(user, category);
		if (modLevel < 2) return rmsg("Insufficient permissions", 400);

		const game = await Game.findOne({ id: gameId });

		if (game.author !== user.id)
			return rmsg("You are not the author of this game!", 400);

		await ForumCategory.updateOne(
			{ id: categoryId },
			{ $push: { linkedGames: gameId } }
		);

		return rmsg("ok");
	}
);
