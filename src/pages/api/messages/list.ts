import { Message } from "~/models/message";
import { User } from "~/models/user";
import { rdata } from "~/helper/res";
import { route } from "~/helper/route";

export const GET = route(
	{
		auth: true,
	},
	async ({ locals }) => {
		const { user } = locals.auth;

		const messages = await Message.find({
			to: user.id,
			deleted: false,
		}).sort({
			createdAt: -1,
		});
		const users = new Set();
		messages.forEach(m => users.add(m.from));

		const userMap = {};
		const userList = await User.find({ id: { $in: [...users.values()] } });

		userList.forEach(u => (userMap[u.id] = [u.name, u.pfpTimestamp]));

		return rdata({
			messages: messages.map(m => ({
				from: m.from,
				fromName: userMap[m.from]?.[0] ?? "[deleted user]",
				fromPic: userMap[m.from]?.[1],
				fromDeleted: userMap[m.from] === undefined,
				title: m.title,
				read: m.read,
				id: m.id,
				createdAt: +m.createdAt,
			})),
		});
	}
);
