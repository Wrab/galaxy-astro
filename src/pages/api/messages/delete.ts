import { Message } from "~/models/message";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";

export const GET = route(
	{
		auth: true,
	},
	async ({ locals, url }) => {
		const { user } = locals.auth;

		const id = url.searchParams.get("id");

		const msg = await Message.findOne({ _id: id, deleted: false });
		if (!msg) return rmsg("Message not found", 404);

		if (msg.to !== user.id)
			return rmsg("You are not the recipient of that message", 401);

		await msg.updateOne({
			deleted: true,
		});

		return rmsg("done");
	}
);
