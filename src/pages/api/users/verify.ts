import { UserAuth, fromEmail } from "~/models/userauth";
import { months, userUnder13 } from "~/helper/coppa";
import { rfu, rmsg } from "~/helper/res";
import { UserParent } from "~/models/userparent";
import { route } from "~/helper/route";
import { sendMail } from "~/helper/mail";
import { z } from "zod";

const schema = z.object({
	code: z.string(),
	parentEmail: z.string().email().optional(),
});

export const POST = route(
	{
		schema,
	},
	async ({ locals }, { code, parentEmail }) => {
		const { loggedIn, needsVerification, user } = locals.auth;
		if (loggedIn && !needsVerification)
			return rfu("You do not meet verification criteria", 403);

		const foundUser = await UserAuth.findOne({
			emailVerified: code.trim(),
		});
		if (!foundUser) return rmsg("Wrong code", 400);

		if (userUnder13(foundUser) && user === undefined)
			return rfu(
				"Please enter your verification code while logged in",
				403
			);

		if (userUnder13(foundUser) && parentEmail === undefined)
			return rfu("Please enter an email address", 403);

		// TODO BIG code duplication here with src/pages/api/users/setBirthday.ts. Could this be stuck into a helper function?
		if (userUnder13(foundUser)) {
			const existingParentUser = await fromEmail(parentEmail);
			if (
				existingParentUser !== null &&
				existingParentUser.id === foundUser.id
			)
				return rfu(
					"Please specify an email address other than your own",
					403
				);

			const parentDoc = new UserParent({
				user: foundUser.id,
				parentEmail,
			});

			await parentDoc.save();

			await sendMail(
				parentEmail,
				"[Action Needed] Your child has created an account on galaxy.click",
				{
					plaintext: `Hello,

We're sending you this email to let you know that your child has created an account on galaxy.click and designated this email as belonging to a parent or guardian. From here, it's up to you if you want to let them use the site or not.

The account was created with the following information:
username -- ${user.name}
email address -- ${foundUser.email}
birth month -- ${months[foundUser.birthMonth]} ${foundUser.birthYear}

**What is galaxy.click?**

galaxy.click is a social website for games that can be played in a web browser. It contains no advertisements or 18+ content.

**Is galaxy.click safe for children?**

While your child is under 13 and logged in, all social features (comments, chat, forums, private messages) will be hidden from the site. The main feature of the site is playing games posted by other users--these games are moderated to make sure they do not contain *18+ content*, but will still be visible to your child. Because the site is mostly targeted towards people over 13, games may contain content you still don't want your child to see, such as profanity. They may also contain links to off-site discussion boards that are not moderated by us.

**What happens if I accept?**

Your child will be able to use certain features on galaxy.click, such as rating games, tracking their playtime, and saving progress between devices.

**What happens if I reject?**

Your child will be logged out of their account. Logged out users may view social features on the site, such as comments, chat, and the forums. If you do not want your child on galaxy.click at all, it may be worth discussing with them directly or setting up parental controls to block the site.

To ACCEPT, go to https://galaxy.click/coppa?code=${parentDoc.parentCode}&action=2

To REJECT, go to https://galaxy.click/coppa?code=${parentDoc.parentCode}&action=1

For further inquiries, please contact support@galaxy.click.
`,
					html: `<p>Hello,</p>
<p>We're sending you this email to let you know that your child has created an account on galaxy.click and designated this email as belonging to a parent or guardian. From here, it's up to you if you want to let them use the site or not.</p>
<p>
The account was created with the following information:<br/>
username -- ${user.name}<br/>
email address -- ${foundUser.email}<br/>
birth month -- ${months[foundUser.birthMonth]} ${foundUser.birthYear}<br/>
</p>
<b>What is galaxy.click?</b>
<p>galaxy.click is a social website for games that can be played in a web browser. It contains no advertisements or 18+ content.</p>
<b>Is galaxy.click safe for children?</b>
<p>While your child is under 13 and logged in, all social features (comments, chat, forums, private messages) will be hidden from the site. The main feature of the site is playing games posted by other users--these games are moderated to make sure they do not contain <i>18+ content</i>, but will still be visible to your child. Because the site is mostly targeted towards people over 13, games may contain content you still don't want your child to see, such as profanity. They may also contain links to off-site discussion boards that are not moderated by us.</p>
<b>What happens if I accept?</b>
<p>Your child will be able to use certain features on galaxy.click, such as rating games, tracking their playtime, and saving progress between devices.</p>
<b>What happens if I reject?</b>
<p>Your child will be logged out of their account. Logged out users may view social features on the site, such as comments, chat, and the forums. If you do not want your child on galaxy.click at all, it may be worth discussing with them directly or setting up parental controls to block the site.</p>
<a href="https://galaxy.click/coppa?code=${parentDoc.parentCode}&action=2" style="color: white;"><div style="background: black; padding: 1em; display: inline-flex;">Accept</div></a>
<a href="https://galaxy.click/coppa?code=${parentDoc.parentCode}&action=1" style="color: white;"><div style="background: black; padding: 1em; display: inline-flex;">Reject</div></a>

<p>For further inquiries, please contact support@galaxy.click.</p>
`,
				}
			);
		}

		foundUser.emailVerified = true;
		await foundUser.save();

		return rmsg("Welcome to Galaxy!", 200);
	}
);
