import { rdata, rmsg } from "~/helper/res";
import { User } from "~/models/user";
import { route } from "~/helper/route";
import sharp from "sharp";
import { updatePfps } from "~/helper/img";

export const POST = route(
	{
		auth: true,
		notMuted: true,
		notUnderage: true,
	},
	async ({ locals, request }) => {
		const { user } = locals.auth;

		const req = await request.formData();

		const file = req.get("image");
		if (file === null) return rmsg("No image provided", 400);
		if (typeof file === "string") return rmsg("Invalid image", 400);
		if (file.size > 1024 * 1024 * 8)
			return rmsg("Please submit an image under 8 MB", 400);

		// This is super stupid.
		// To put it simply, I hate this code.
		// But, alas, it works.
		//   File -> ArrayBuffer -> Buffer -> Sharp
		const abuf = await file.arrayBuffer();
		const buf = Buffer.from(abuf);

		let oops = false;

		const original = user.pfpTimestamp;
		const replaced = Date.now();
		await User.updateOne({ id: user.id }, { pfpTimestamp: replaced });

		await updatePfps(sharp(buf), user.id, replaced, original).catch(e => {
			oops = e;
		});
		if (oops)
			return rmsg(
				`Something went wrong with image processing. ${oops}`,
				500
			);

		return rdata({ message: "success", time: replaced });
	}
);
