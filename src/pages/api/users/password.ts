import { UserAuth, generateTokenHeaders } from "~/models/userauth";
import bcrypt from "bcryptjs";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	new: z
		.string({
			invalid_type_error: "New password not provided",
		})
		.min(10, "New password is too short (min: 10)")
		.max(128, "New password is too long (max: 128)"),
	old: z
		.string({
			invalid_type_error: "Old password not provided",
		})
		.min(3, "Old password is too short (min: 3)")
		.max(128, "New password is too long (max: 128)"),
});

export const POST = route(
	{
		auth: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const userDoc = await UserAuth.findOne({ id: user.id });

		const isCorrect = await bcrypt.compare(req.old, userDoc.password);
		if (!isCorrect) return rmsg("Old password is not correct", 400);

		const isSame = await bcrypt.compare(req.new, userDoc.password);
		if (isSame) return rmsg("New password matches old password", 400);

		userDoc.password = await bcrypt.hash(req.new, 12);
		userDoc.sessionState++;

		await userDoc.save();

		return new Response(
			JSON.stringify({
				message: "Password updated!",
			}),
			{
				status: 200,
				headers: generateTokenHeaders(userDoc),
			}
		);
	}
);
