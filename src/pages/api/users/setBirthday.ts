import { UserAuth, fromEmail } from "~/models/userauth";
import { months, userUnder13 } from "~/helper/coppa";
import { rfu, rmsg } from "~/helper/res";
import { UserParent } from "~/models/userparent";
import { route } from "~/helper/route";
import { sendMail } from "~/helper/mail";
import { z } from "zod";

const schema = z.object({
	// TODO Technically not very good but hopefully shouldn't cause any issues :3
	year: z.coerce.number().int().min(1900).max(new Date().getFullYear()),
	month: z.coerce.number().int().min(0).max(11),
	email: z.string().email().or(z.string().max(0)),
});

export const POST = route(
	{
		notMuted: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user, needsVerification } = locals.auth;
		if (user === undefined || !needsVerification)
			return rmsg("Birthday selection not applicable", 400);

		const userAuth = await UserAuth.findOne({ id: user.id });

		userAuth.birthYear = req.year;
		userAuth.birthMonth = req.month;
		if (!userUnder13(userAuth))
			await UserAuth.updateOne(
				{ id: user.id },
				{ $unset: { parentState: 1 } }
			);
		else if (req.email === "")
			return rfu("Please specify your parent's email address", 403);
		else {
			// TODO BIG code duplication here with src/pages/api/users/verify.ts. Could this be stuck into a helper function?
			const existingParentUser = await fromEmail(req.email);
			if (
				existingParentUser !== null &&
				existingParentUser.id === userAuth.id
			)
				return rfu(
					"Please specify an email address other than your own",
					403
				);

			const parentDoc = new UserParent({
				user: userAuth.id,
				parentEmail: req.email,
			});

			await parentDoc.save();

			await sendMail(
				req.email,
				"[Action Needed] Your child has created an account on galaxy.click",
				{
					plaintext: `Hello,

We're sending you this email to let you know that your child has created an account on galaxy.click and designated this email as belonging to a parent or guardian. From here, it's up to you if you want to let them use the site or not.

The account was created with the following information:
username -- ${user.name}
email address -- ${userAuth.email}
birth month -- ${months[userAuth.birthMonth]} ${userAuth.birthYear}

**What is galaxy.click?**

galaxy.click is a social website for games that can be played in a web browser. It contains no advertisements or 18+ content.

**Is galaxy.click safe for children?**

While your child is under 13 and logged in, all social features (comments, chat, forums, private messages) will be hidden from the site. The main feature of the site is playing games posted by other users--these games are moderated to make sure they do not contain *18+ content*, but will still be visible to your child. Because the site is mostly targeted towards people over 13, games may contain content you still don't want your child to see, such as profanity. They may also contain links to off-site discussion boards that are not moderated by us.

**What happens if I accept?**

Your child will be able to use certain features on galaxy.click, such as rating games, tracking their playtime, and saving progress between devices.

**What happens if I reject?**

Your child will be logged out of their account. Logged out users may view social features on the site, such as comments, chat, and the forums. If you do not want your child on galaxy.click at all, it may be worth discussing with them directly or setting up parental controls to block the site.

To ACCEPT, go to https://galaxy.click/coppa?code=${parentDoc.parentCode}&action=2

To REJECT, go to https://galaxy.click/coppa?code=${parentDoc.parentCode}&action=1
`,
					html: `<p>Hello,</p>
<p>We're sending you this email to let you know that your child has created an account on galaxy.click and designated this email as belonging to a parent or guardian. From here, it's up to you if you want to let them use the site or not.</p>
<p>
The account was created with the following information:<br/>
username -- ${user.name}<br/>
email address -- ${userAuth.email}<br/>
birth month -- ${months[userAuth.birthMonth]} ${userAuth.birthYear}<br/>
</p>
<b>What is galaxy.click?</b>
<p>galaxy.click is a social website for games that can be played in a web browser. It contains no advertisements or 18+ content.</p>
<b>Is galaxy.click safe for children?</b>
<p>While your child is under 13 and logged in, all social features (comments, chat, forums, private messages) will be hidden from the site. The main feature of the site is playing games posted by other users--these games are moderated to make sure they do not contain <i>18+ content</i>, but will still be visible to your child. Because the site is mostly targeted towards people over 13, games may contain content you still don't want your child to see, such as profanity. They may also contain links to off-site discussion boards that are not moderated by us.</p>
<b>What happens if I accept?</b>
<p>Your child will be able to use certain features on galaxy.click, such as rating games, tracking their playtime, and saving progress between devices.</p>
<b>What happens if I reject?</b>
<p>Your child will be logged out of their account. Logged out users may view social features on the site, such as comments, chat, and the forums. If you do not want your child on galaxy.click at all, it may be worth discussing with them directly or setting up parental controls to block the site.</p>
<a href="https://galaxy.click/coppa?code=${parentDoc.parentCode}&action=2" style="color: white;"><div style="background: black; padding: 1em; display: inline-flex;">Accept</div></a>
<a href="https://galaxy.click/coppa?code=${parentDoc.parentCode}&action=1" style="color: white;"><div style="background: black; padding: 1em; display: inline-flex;">Reject</div></a>
`,
				}
			);
		}

		await userAuth.save();

		return rmsg("Okay", 200);
	}
);
