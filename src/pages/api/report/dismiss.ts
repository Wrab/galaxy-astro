import { Report } from "~/models/report";
import { User } from "~/models/user";
import { adminRoute } from "~/helper/route";
import { logAction } from "~/helper/adminHelper";

export const POST = adminRoute({}, async (req, _data, mod) => {
	const { report } = await req.json();

	const reportDoc = await Report.findByIdAndUpdate(report, {
		resolved: true,
	});

	const reportAuthor = await User.findOne({ id: reportDoc.id });
	await logAction(
		mod.id,
		"report",
		`dismissed report by ${reportAuthor.name} (${reportAuthor.id})`,
		`the report was about ${reportDoc.part} | ${reportDoc.reason}`,
		{
			userId: reportAuthor.id,
			report: reportDoc,
		}
	);
});
