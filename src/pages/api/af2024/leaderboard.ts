import { rdata, rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { AF2024Participant } from "~/models/af2024participant";
import { User } from "~/models/user";

export const GET = route({}, async () => {
	const types = [{ clicks: -1 }, { first: -1 }, { last: -1 }];
	const [byClicks, byFirst, byLast] = await Promise.all(
		types.map(type =>
			AF2024Participant.find(
				{},
				{ _id: 0, __v: 0 },
				{ sort: type, limit: 20, lean: true }
			)
		)
	);

	const uIds = new Set<number>();
	[byClicks, byFirst, byLast].forEach(leaderboard => {
		leaderboard.forEach(placement => uIds.add(placement.user));
	});

	const accounts = await User.find({ id: { $in: Array.from(uIds) } });
	const users = {};
	accounts.forEach(
		user =>
			(users[user.id] = {
				name: user.name,
				pfp: user.pfpTimestamp,
				flair: user.equippedFlair,
			})
	);

	return rdata({ users, leaderboards: { byClicks, byFirst, byLast } });
});
