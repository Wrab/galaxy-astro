import { Schema, model } from "mongoose";
import type { ICategoryModerator } from "~/types";

const CategoryModeratorSchema = new Schema<ICategoryModerator>({
	user: Number,
	category: Number,
	perms: Number,
});

export const CategoryModerator = model<ICategoryModerator>(
	"CategoryModerators",
	CategoryModeratorSchema
);
