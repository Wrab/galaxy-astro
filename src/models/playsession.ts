import { Schema, model } from "mongoose";
import type { IPlaySession } from "~/types";

const PlaySessionSchema = new Schema<IPlaySession>({
	user: Number,
	game: Number,
	startTime: {
		type: Number,
		default: () => Date.now(),
	},
	endTime: {
		type: Number,
		default: () => Date.now(),
	},
	minutes: {
		type: Number,
		default: 0,
	},
});

export const PlaySession = model<IPlaySession>(
	"PlaySession",
	PlaySessionSchema
);
