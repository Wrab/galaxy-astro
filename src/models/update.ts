import { Schema, model } from "mongoose";
import type { IUpdate } from "~/types";
import { z } from "zod";

const UpdateSchema = new Schema<IUpdate>(
	{
		game: Number,
		id: Number,
		changelog: {
			type: String,
			minLength: 1,
			maxLength: 8192,
			required: true,
		},
		name: {
			type: String,
			maxLength: 128,
			required: false,
		},
		version: {
			type: String,
			maxLength: 32,
			required: false,
		},
	},
	{
		timestamps: true,
	}
);

export const Update = model<IUpdate>("Updates", UpdateSchema);

export const updateSchema = z.object({
	changelog: z
		.string()
		.trim()
		.min(1, "Add a changelog before updating!")
		.max(8192, "Changelog too long"),
	version: z.string().trim().max(32, "Version number too long").optional(),
	name: z.string().trim().max(128, "Update name too long").optional(),
	game: z.number().gt(0).int(),
	silent: z.boolean().default(false),
});
