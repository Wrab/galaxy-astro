import { defineConfig } from "vite";
import { fileURLToPath } from "url";

export default defineConfig({
	ssr: {
		external: [],
		noExternal: [],
	},
	build: {
		commonjsOptions: {
			transformMixedEsModules: true,
		},
	},
	resolve: {
		alias: {
			"~": fileURLToPath(new URL("./src", import.meta.url)),
		},
	},
});
