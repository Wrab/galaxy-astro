# contributing

Hey! Thanks for your interest in contributing to galaxy.

Don't know what to work on? Look through [the issue tracker][issues] or search the repository for "TODO" or "FIXME" and tackle one that suits your fancy.

If you need any help, feel free to come and chat with us in our [Matrix space][matrix] and [Discord server][discord].

## Setup

1. Install [pnpm]
2. Install [MongoDB][mongodb] (or set up a MongoDB Atlas account, just acquire a MongoDB database somehow)
3. Clone and `cd` into the repository
4. Install dependencies: `pnpm install`
5. Initialize configuration and data directories: `node init.js`

## Running the server

### Development

1. (Optional, if using local MongoDB) Start MongoDB
2. Run `pnpm dev`

#### Running WebSocket Server (Chat)

While the development server starts the WebSocket server for you, you must start it manually when running the preview server.

1. Build the site: `pnpm build`
2. Run `node express.js` and `node ws.js dev` parallel
3. Site can be found on port 3000

### Production

Galaxy runs in production at <https://galaxy.click>.

1. Run `pnpm build`
2. Set up your SMTP server of choice (AWS SES has a good free tier)
3. Move `dist` and `dist-router` to `prod` and `prod-ws` respectively
4. Run `node express.js` and `node ws.js` in parallel
   - `express.js` handles all connections to port 3000 (the web server)
   - `ws.js` handles all connections to port 8443 (the WebSocket server)

## Code structure

- `public/`: Static files such as image assets and styles (hey, that rhymes!)
- `src/`: Source code
  - `backend/`: WebSocket server implementation
  - `components/`: Reusable components (Astro and Svelte)
  - `helper/`: Utitlity/helper functions
  - `layouts/`: Page layouts (Astro)
  - `middleware/`: Astro middleware (authentication)
  - `models/`: Database schemas
  - `pages/`: Astro page routes (the actual website)
    - `api/`: API routes
  - `trpc/`: WebSocket client implementation

## Linting and formatting

Currently, due to a [long-standing issue with Astro's Prettier plugin](https://github.com/withastro/prettier-plugin-astro/issues/359) and the many failed ESLint rules... don't take these tools too seriously.

Just remember to indent with tabs :)

## [Issues][issues]

There is no set structure for issue reporting, but please try to be clear and provide as much information as you can. You may also report issues in the [`#galaxy-feedback` Discord channel][discord] or [Matrix][matrix].

### [Labels][labels]

The issue tracker is categorized using labels. Here are some of the most important ones:

Priority:

- [`priority::3`](https://gitlab.com/yhvr/galaxy-astro/-/issues/?label_name%5B%5D=priority%3A%3A3): Low priority
- [`priority::2`](https://gitlab.com/yhvr/galaxy-astro/-/issues/?label_name%5B%5D=priority%3A%3A2): Medium priority
- [`priority::1`](https://gitlab.com/yhvr/galaxy-astro/-/issues/?label_name%5B%5D=priority%3A%3A1): High priority

Difficulty:

- [`difficulty::3`](https://gitlab.com/yhvr/galaxy-astro/-/issues/?label_name%5B%5D=difficulty%3A%3A3): Easy difficulty, small size
- [`difficulty::2`](https://gitlab.com/yhvr/galaxy-astro/-/issues/?label_name%5B%5D=difficulty%3A%3A2): Medium difficulty, average size
- [`difficulty::1`](https://gitlab.com/yhvr/galaxy-astro/-/issues/?label_name%5B%5D=difficulty%3A%3A1): Hard difficulty, large size

## [Merge requests][merge-requests]

There is no strict format for commit descriptions, but it is preferred that you accurately describe the content of the commit. [See this relevant xkcd](https://xkcd.com/1296/).

[Here's a tip][closing-issues-automatically]: In a merge request, if its description or a constituent commit includes "closes #\<issue\>", that issue will automatically close when the MR is merged.

[discord]: https://galaxy.click/discord
[matrix]: https://matrix.to/#/#galaxy:glxy.chat

[issues]: https://gitlab.com/yhvr/galaxy-astro/-/issues
[labels]: https://gitlab.com/yhvr/galaxy-astro/-/labels
[merge-requests]: https://gitlab.com/yhvr/galaxy-astro/-/merge_requests

[closing-issues-automatically]: https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically
[mongodb]: https://www.mongodb.com/
[pnpm]: https://pnpm.io/
