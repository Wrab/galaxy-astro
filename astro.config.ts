import { defineConfig } from "astro/config";
import { fileURLToPath } from "url";
import trpcWSS from "./src/backend/integration";
import viteConfig from "./vite.config";

import react from "@astrojs/react";
import svelte from "@astrojs/svelte";

import node from "@astrojs/node";

export default defineConfig({
	integrations: [
		svelte(),
		react(),
		trpcWSS({
			port: 8443,
			routerFilePath: fileURLToPath(
				new URL("./src/backend/router.ts", import.meta.url)
			),
			routerName: "socketRouter",
			createContextName: "socketContext",
		}),
	],
	adapter: node({
		mode: "middleware",
	}),
	output: "server",
	outDir: "dist/main",
	server: {
		// headers: {
		// 	"Content-Security-Policy": "frame-ancestors 'none'",
		// },
	},
	vite: viteConfig,
	site: "https://galaxy.click",
	compressHTML: true,
	redirects: {
		"/game/[id]": "/play/[id]",
		"/you": "/settings",
		"/themes": "/settings#themes",
		"/filters": "/settings#filters",
	},
});
