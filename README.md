```
                     oooo
                     `888
 .oooooooo  .oooo.    888   .oooo.   oooo    ooo oooo    ooo
888' `88b  `P  )88b   888  `P  )88b   `88b..8P'   `88.  .8'
888   888   .oP"888   888   .oP"888     Y888'      `88..8'
`88bod8P'  d8(  888   888  d8(  888   .o8"'88b      `888'
`8oooooo.  `Y888""8o o888o `Y888""8o o88'   888o     .8'
d"     YD                                        .o..P'
"Y88888P'                                        `Y8P'
```

[*the place for incremental games.*](https://galaxy.click)

a website for uploading games (mostly) from the "incremental" genre. made with astro, svelte, typescript, and mongoose.

devs: upload your incremental games, get more exposure and feedback.

players: find new incrementals, give feedback, and talk with other fans. no need to join a dozen discord servers.

[discord](https://galaxy.click/discord)

## dev

if you just want to see galaxy in production, go to <https://galaxy.click>.

for information about development, though, see [the contributing guide](CONTRIBUTING.md).

---

## contributors

galaxy is only this good--or exists at all--thanks to all of you.

### code
* ducdat0507
* wackbyte
* yahtzee
* incremental_gamer (circle-gon)

### special thanks
* Kongregate
* r/incremental_games
* smileyrandom
* ThePaperPilot
* Grant Gryczan
* Crimson406
* you :D
